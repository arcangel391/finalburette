import React, { Component } from 'react'
import './about.css'
import fb from '../assets/icons/facebook.svg'
import ig from '../assets/icons/instagram.svg'
import git from '../assets/icons/github.svg'
import pin from '../assets/icons/pin.svg'
import phone from '../assets/icons/phone.svg'
import mightycoder from '../assets/mightycoder.svg'
import rect from '../assets/icons/react.svg'
import Bar from './Bar'
import csa from '../assets/icons/csa.png'

const languages = [
    { 
    icon: rect,
    name:' Python',
    level:'50'
    },
    { 
        icon: rect,
        name:' Java',
        level:'65'
        },
        { 
            icon: rect,
            name:' HTML',
            level:'78'
            },
            { 
                icon: rect,
                name:' C++',
                level:'75'
                },
                { 
                    icon: rect,
                    name:' PHP',
                    level:'75'
                    },
                    { 
                        icon: rect,
                        name:' JavaScript',
                        level:'78'
                        },
                        { 
                            icon: rect,
                            name:' SQL',
                            level:'87'
                            }
            
]

const tools = [
    { 
    icon: rect,
    name:' Photoshop',
    level:'90'
    },
    { 
        icon: rect,
        name:' Android Studio',
        level:'75'
        },
        { 
            icon: rect,
            name:' CSS',
            level:'80'
            }
]


export default class Cs extends Component {
    render() {
        return (
            <div className="app">
            <div className="container">
                <div className="row app__row">
                    <div className="col-lg-3">
                        <div className="ap__sidebar">
                            <div>
                                <img src={csa} alt="avt" className="sidebar__image"/>
                                <div className="sidebar__name">Christian Saavedra</div>
                                <div className="sidebar__item sidebar__title">Virtual Assistant</div>
                                <figure className="sidebar__social-icons">
                                    <a href="#"><img src={fb} alt="fb" className="sidebar__icon"/></a>
                                    <a href="#"><img src={ig} alt="ig" className="sidebar__icon"/></a>
                                    <a href="#"><img src={git} alt="git" className="sidebar__icon"/></a>
                                </figure>

                                <div className="contact">
                                    <div className="sidebar__item">saavedra.christian@gmail.com</div>
                                    <img src={pin} alt="git" className="sidebar__icon"/>Quezon City, Philippines
                                    
                                    <div className="sidebar__item"> <img src={phone} alt="git" className="sidebar__icon"/>09292706525</div>
                                </div>

                              

                                
                            </div>
                        </div>
                    </div>




                    <div className="col-lg-9 ap__main-content">
                        <div className="navbar__active">About</div>

                        <div className="container__resume">
                        <div className="row">
                                <div className="col-lg-6 resume__card">
                                    <h4 className="resume-card__heading">
                                        Education
                                    </h4>
                                    <div className="resume-card__body">
                                        <h5 className="resume-card__tile">
                                            Information Techonology
                                        </h5>
                                        <p className="resume-card__name">
                                            Technological Institute of the Philippines - Manila
                                        </p>
                                        <p className="resume-card__details">
                                            2018 - Present

                                        </p>
                                    </div>
                                </div>
                                <div className="col-lg-6 resume__card">
                                        <h4 className="resume-card__heading">
                                            Motto in Life
                                        </h4>
                                        <div className="resume-card__body">
                                            <h5 className="resume-card__tile">
                                            "Be strong and courageous"
                                            </h5>
                                            <p className="resume-card__name">
                                                
                                            </p>
                                            <p className="resume-card__details">

                                        </p>
                                    </div>
                                </div>

                            </div>


                            <div className="row">
                                <div className="col-lg-6 resume-languages">
                                    <h5 className="resume-language__heading">
                                        Language and Framework
                                    </h5>
                                    <div className="resume-language__body mt-3">
                                        {
                                            languages.map(
                                                Language=><Bar value={Language}/>
                                            )
                                        }
                                    </div>
                                </div>
                                <div className="col-lg-6 resume-languages">
                                    <h5 className="resume-language__heading">
                                        Tools and Software
                                    </h5>
                                    <div className="resume-language__body mt-3">
                                        {
                                            tools.map(
                                                tool=><Bar value={tool}/>
                                            )
                                        }
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
            
        </div>
        )
    }
}
