import React, { Component } from 'react'
import {Navbar,Nav,NavDropdown,Form,FormControl,Button,Container} from 'react-bootstrap'
import {
    BrowserRouter as Router,
    Routes,
    Switch,
    Route,
    Link
  } from "react-router-dom";
import App from '../App';
import About from './About';
import Cedc from './Cedc';
import Cs from './Cs';
import Home from './Home';
import Lim from './Lim';
import Mark from './Mark';
import Ryan from './Ryan';
import burette from '../assets/icons/burette.png'
  

export default class NavbarComp extends Component {
    render() {
        return (    
        <Router>
            <div>
                <Navbar expand="lg" bg="light" variant="light">
                    
  <Container fluid>
    <Navbar.Brand  as={Link} to={"/"}>
    <img src={burette} alt="avt" className="image_nav"/>
    </Navbar.Brand>
    <Navbar.Toggle aria-controls="navbarScroll" />
   
    <Navbar.Collapse id="navbarScroll">
      <Nav
        className="me-auto my-2 my-lg-0"
        style={{ maxHeight: '100px' }}
        navbarScroll
      >
        
        <Nav.Link  as={Link} to={"/about"}>Quintero</Nav.Link>
        <Nav.Link  as={Link} to={"/mark"}>Patropez</Nav.Link>    
        <Nav.Link  as={Link} to={"/ryan"}>Dusong</Nav.Link>   
        <Nav.Link  as={Link} to={"/cedc"}>Cruz</Nav.Link>   
        <Nav.Link  as={Link} to={"/lim"}>Lim</Nav.Link>   
        <Nav.Link  as={Link} to={"/cs"}>Saavedra</Nav.Link>   
        
      </Nav>
     
    </Navbar.Collapse>
   
  </Container>
  
</Navbar>
            </div>
            <div>
         <Routes>
         <Route path='/about' element={<About/>} />
         <Route path='/home' element={<Home/>} />
         <Route path='/mark' element={<Mark/>} />
         <Route path='/ryan' element={<Ryan/>} />
         <Route path='/cedc' element={<Cedc/>} />
         <Route path='/lim' element={<Lim/>} />
         <Route path='/cs' element={<Cs/>} />
         <Route path='/' element={<Home/>} />
  
        </Routes>
    
            </div>
            </Router>
          
        )
    }
}
