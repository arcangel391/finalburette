import React, { Component } from 'react'
import './about.css'
import fb from '../assets/icons/facebook.svg'
import ig from '../assets/icons/instagram.svg'
import git from '../assets/icons/github.svg'
import pin from '../assets/icons/pin.svg'
import phone from '../assets/icons/phone.svg'
import mightycoder from '../assets/mightycoder.svg'
import rect from '../assets/icons/react.svg'
import Bar from './Bar'
import mpat from '../assets/icons/mpat.png'

const languages = [
    { 
    icon: rect,
    name:' Python',
    level:'40'
    },
    { 
        icon: rect,
        name:' Java',
        level:'80'
        },
        { 
            icon: rect,
            name:' HTML',
            level:'60'
            },
            { 
                icon: rect,
                name:' C++',
                level:'75'
                },
                { 
                    icon: rect,
                    name:' PHP',
                    level:'83'
                    },
                    { 
                        icon: rect,
                        name:' JavaScript',
                        level:'60'
                        },
                        { 
                            icon: rect,
                            name:' SQL',
                            level:'65'
                            }
            
]

const tools = [
    { 
    icon: rect,
    name:' Photoshop',
    level:'89'
    },
    { 
        icon: rect,
        name:' Android Studio',
        level:'75'
        },
        { 
            icon: rect,
            name:' CSS',
            level:'80'
            }
]


export default class Mark extends Component {
    render() {
        return (
            <div className="app">
            <div className="container">
                <div className="row app__row">
                    <div className="col-lg-3">
                        <div className="ap__sidebar">
                            <div>
                                <img src={mpat} alt="avt" className="sidebar__image"/>
                                <div className="sidebar__name">Mark Patropez</div>
                                <div className="sidebar__item sidebar__title">Front-end Developer</div>
                                <figure className="sidebar__social-icons">
                                    <a href="#"><img src={fb} alt="fb" className="sidebar__icon"/></a>
                                    <a href="#"><img src={ig} alt="ig" className="sidebar__icon"/></a>
                                    <a href="#"><img src={git} alt="git" className="sidebar__icon"/></a>
                                </figure>

                                <div className="contact">
                                    <div className="sidebar__item">mcpatropezgmail.com</div>
                                    <img src={pin} alt="git" className="sidebar__icon"/>Manila, Philippines
                                    
                                    <div className="sidebar__item"> <img src={phone} alt="git" className="sidebar__icon"/>09610604198</div>
                                </div>

                              

                                
                            </div>
                        </div>
                    </div>




                    <div className="col-lg-9 ap__main-content">
                        <div className="navbar__active">About</div>

                        <div className="container__resume">
                        <div className="row">
                                <div className="col-lg-6 resume__card">
                                    <h4 className="resume-card__heading">
                                        Education
                                    </h4>
                                    <div className="resume-card__body">
                                        <h5 className="resume-card__tile">
                                            Information Techonology
                                        </h5>
                                        <p className="resume-card__name">
                                            Technological Institute of the Philippines - Manila
                                        </p>
                                        <p className="resume-card__details">
                                            2018 - Present

                                        </p>
                                    </div>
                                </div>
                                <div className="col-lg-6 resume__card">
                                        <h4 className="resume-card__heading">
                                            Motto in Life
                                        </h4>
                                        <div className="resume-card__body">
                                            <h5 className="resume-card__tile">
                                                "Always Keep Moving Forward"
                                            </h5>
                                            <p className="resume-card__name">
                                                
                                            </p>
                                            <p className="resume-card__details">

                                        </p>
                                    </div>
                                </div>

                            </div>


                            <div className="row">
                                <div className="col-lg-6 resume-languages">
                                    <h5 className="resume-language__heading">
                                        Language and Framework
                                    </h5>
                                    <div className="resume-language__body mt-3">
                                        {
                                            languages.map(
                                                Language=><Bar value={Language}/>
                                            )
                                        }
                                    </div>
                                </div>
                                <div className="col-lg-6 resume-languages">
                                    <h5 className="resume-language__heading">
                                        Tools and Software
                                    </h5>
                                    <div className="resume-language__body mt-3">
                                        {
                                            tools.map(
                                                tool=><Bar value={tool}/>
                                            )
                                        }
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
            
        </div>
        )
    }
}
